import React, {Component} from 'react';
import {Text,StyleSheet,View} from 'react-native';



   // const { viewStyle, tituloHeader } = styles;
const CardSection = (props) => {

    return (
        <View style={Styles.containerStyle}>
        {props.children}
        </View>
    );

} ;

const Styles = {

    containerStyle : {
        borderBottomWidth: 1,
        padding: 5,
        backgroundColor: '#fff',
        justifyContent: 'flex-start',
        flexDirection: 'row',
        borderColor : '#ddd',
        position: 'relative'
    }
};


export default CardSection;