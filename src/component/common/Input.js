import React, {Component} from 'react';
import {Text,TextInput,StyleSheet,View} from 'react-native';



   // const { viewStyle, tituloHeader } = styles;
const Input = ({label,value,onChangeText,placeHolder,secureTextEntry}) => {

const{inputStyle,labelStyle,containerStyle} = styles;

    return (
        <View style={containerStyle}>
            <Text style={labelStyle}>{label}</Text> 
            <TextInput style={inputStyle} underlineColorAndroid="transparent" 
                     autocorrect = {false}
                     placeholder = {placeHolder}
                     value={value}
                     secureTextEntry ={secureTextEntry}
                     onChangeText={onChangeText} />
        </View>
    );

} ;

const styles = {

    inputStyle : {
        color: '#000',
        paddingRight: 5,
        paddingLeft: 5,
        fontSize: 18,
        lineHeight: 23,
        flex:2,
        height:40,
        width: 200
    },
    labelStyle :{
        fontSize: 18,
        paddingLeft: 20,
        flex: 1
    },
    containerStyle: {
        height: 40,
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center'

    }
};


export default Input;