import React from 'react';
import {Text,StyleSheet,View} from 'react-native';

const Header = (props) => {

    const { viewStyle, tituloHeader } = styles;

    return (
        <View style={styles.viewStyle}>
            <Text style={styles.tituloHeader}>{props.textoHeader}</Text>
        </View>
    );

} ;

const styles = {
    viewStyle: {
        backgroundColor : '#F8F8F8',
        alignItems: 'center',
        justifyContent: 'center',
        height: 80,
        paddingTop: 25,
        shadowColor: '#000',
        shadowOffset : {width: 2,height:4 },
        shadowOpacity: 1,
        elevation: 2,
        position: 'relative'
       },
    tituloHeader: {
     fontSize : 30
    }
  
  };
  


export default Header;