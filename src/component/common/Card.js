import React, {Component} from 'react';
import {Text,StyleSheet,View} from 'react-native';



   // const { viewStyle, tituloHeader } = styles;
const Card = (props) => {

    return (
        <View style={Styles.containerStyle}>
        {props.children}
        </View>
    );

} ;

const Styles = {

    containerStyle : {
        borderWidth :1,
        borderRadius : 2,
        borderColor : '#DDD',
        borderBottomWidth: 0,
        shadowColor: '#000',
        shadowOffset : {width: 0,height:4 },
        shadowOpacity: 1,
        shadowRadius: 2,
        elevation: 1,
        marginLeft: 5,
        marginRight: 5,
        marginTop : 10
    }
};


export default Card;