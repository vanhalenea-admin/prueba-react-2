import React, {Component } from 'react';
import {TextInput,StyleSheet,View, Text} from 'react-native';
import Card from './common/Card';
import CardSection from './common/CardSection';
import Button from './common/Button';
import Input from './common/Input';
import firebase from 'firebase';
import Spinner from './common/Spinner';

class LoginForm extends Component {

state = {email: '',password : '', error: '', loading: false};


onButtonPress(){
   let email = this.state.email;
   let password = this.state.password;

   this.setState({error : '', loading : true});

    firebase.auth().signInWithEmailAndPassword(email,password)
    .then(() => {
        this.onLoginSuccess();
    })
    .catch((error2) => {
        
        firebase.auth().createUserWithEmailAndPassword(email,password)
            .then(() => {
                this.onLoginSuccess();
            })
            .catch((error) => {
                this.onLoginFail(error);    
            })
        }
    );
}

renderButton(){
if(this.state.loading)
    return <Spinner size="small" />
else
    return <Button onPress={this.onButtonPress.bind(this)}>Acceder</Button>
}

onLoginFail(error)
{
    this.setState({error : error.message,loading : false});
}

onLoginSuccess()
{
    this.setState({
        email: '',password : '', error: '', loading: false
    });

}
render(){
    const{errorTextStyle} = styles;
    return (
        <View>
             <Card>
                <CardSection> 
                    <Input
                     label = 'Email'
                     placeHolder = 'user@mail.com'
                     value={this.state.email}
                     onChangeText={email => this.setState({email})} />
                </CardSection>
                 <CardSection> 
                 <Input
                     label = 'Password'
                     placeHolder = 'password'
                     value={this.state.password}
                     secureTextEntry ={true}
                     onChangeText={password => this.setState({password})} />
                </CardSection>
                <Text style={errorTextStyle}>{this.state.error}</Text>
                <CardSection> 
                    {this.renderButton()}
                </CardSection>
             </Card> 
        </View>
    );
  }
} 

const styles = {

    errorTextStyle : {
        fontSize: 20,
        alignSelf: 'center',
        color: 'red',
    }
};



export default LoginForm;