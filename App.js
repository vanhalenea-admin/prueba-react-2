import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import Header from './src/component/common/header';
import LoginForm from './src/component/LoginForm';
import Button from './src/component/common/Button';
import CardSection from './src/component/common/CardSection';
import firebase from 'firebase'
import Spinner from './src/component/common/Spinner';

export default class App extends React.Component {

state = {loggedIn : false}

componentWillMount(){

  firebase.initializeApp(
    {
        apiKey: "AIzaSyD59sTAJWQEnH_huJYrYnyKn-O-ij5Xr-0",
        authDomain: "test2-2ab9c.firebaseapp.com",
        databaseURL: "https://test2-2ab9c.firebaseio.com",
        projectId: "test2-2ab9c",
        storageBucket: "test2-2ab9c.appspot.com",
        messagingSenderId: "529524251220"
    }
  );


  firebase.auth().onAuthStateChanged((user) => {

    if(user){
      this.setState({loggedIn : true});
    }else{
      this.setState({loggedIn :false});
    }

  });
}


renderContent(){
switch(this.state.loggedIn){

    case true:
      return (<CardSection><Button onPress={() => firebase.auth().signOut()}>Log off</Button></CardSection>);
    case false:
      return (<LoginForm />) ;
    default:
      return (<Spinner size="small" />);
    }

}

  render() {
    return (
      <View >
       <Header textoHeader = { 'titulo'} />
        {this.renderContent()}
    
      </View>
    );
  }
}


